package com.example.vaccinservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaccinServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaccinServiceApplication.class, args);
    }

}
