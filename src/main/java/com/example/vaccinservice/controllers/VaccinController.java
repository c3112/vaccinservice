package com.example.vaccinservice.controllers;

import com.example.vaccinservice.dto.VaccinDataDTO;
import com.example.vaccinservice.models.Vaccin;
import com.example.vaccinservice.repositories.VaccinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import util.OnCreateVaccinEvent;

import javax.mail.Multipart;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/vaccins")
public class VaccinController {

    @Autowired
    VaccinRepository vaccinRepository;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @GetMapping
    @RequestMapping()
    public List<Vaccin> list() {
        return vaccinRepository.findAll();
    }

    @GetMapping
    @RequestMapping("/{username}")
    public List<Vaccin> get(@PathVariable String username) {
        if (vaccinRepository.findAllByUsername(username).isEmpty()) {
            return null;
        }
        return vaccinRepository.findAllByUsername(username);
    }

    /*@PostMapping("/doVaccin")
    public Object saveVaccin(@RequestBody VaccinDataDTO vaccinBody) {

        Vaccin vaccin = new Vaccin();
        vaccin.setUsername(vaccinBody.getUsername());
        vaccin.setVaccin_date(vaccinBody.getVaccin_date());
        vaccin.setVaccin_type(vaccinBody.getVaccin_type());
        vaccin.setComplet(vaccinBody.getComplet());
        vaccin.setImage(vaccinBody.getImage());

        vaccinRepository.saveAndFlush(vaccin);
        eventPublisher.publishEvent(new OnCreateVaccinEvent("/", vaccin));

        return vaccin;
    }*/

    @PostMapping("/doVaccin")
    public Vaccin saveImage(
            @RequestParam("image") MultipartFile file,
            @RequestParam("username") String username,
            @RequestParam("vaccin_date") String date,
            @RequestParam("vaccin_type") String type,
            @RequestParam("complet") Boolean complet) {

        System.out.println(" === BAD REQUEST ===" + username);
        Vaccin vaccin = new Vaccin();

        try {
            vaccin.setUsername(username);
            vaccin.setVaccin_date(new Date());
            vaccin.setVaccin_type(type);
            vaccin.setComplet(complet);
            vaccin.setImage(file.getBytes());
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't upload vaccin");
        }

        return vaccinRepository.saveAndFlush(vaccin);
    }

}


