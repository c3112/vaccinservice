package com.example.vaccinservice.dto;

import java.util.Date;

public class VaccinDataDTO {

    private String username;
    private String vaccin_type;
    private Date vaccin_date;
    private Boolean complet;
    private Byte[] image;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVaccin_type() {
        return vaccin_type;
    }

    public void setVaccin_type(String vaccin_type) {
        this.vaccin_type = vaccin_type;
    }

    public Date getVaccin_date() {
        return vaccin_date;
    }

    public void setVaccin_date(Date vaccin_date) {
        this.vaccin_date = vaccin_date;
    }

    public Boolean getComplet() {
        return complet;
    }

    public void setComplet(Boolean complet) {
        this.complet = complet;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "VaccinDataDTO{" +
                "username='" + username + '\'' +
                ", vaccin_type='" + vaccin_type + '\'' +
                ", vaccin_date=" + vaccin_date +
                ", complet=" + complet +
//                ", image='" + image + '\'' +
                '}';
    }
}
