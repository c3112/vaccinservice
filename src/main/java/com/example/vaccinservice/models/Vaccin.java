package com.example.vaccinservice.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.bytebuddy.implementation.bind.annotation.Default;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "vaccin")
@Access(AccessType.FIELD)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Vaccin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long vaccin_id;
    private String username;
    private Date vaccin_date;
    private boolean complet;
    private String vaccin_type;
    private byte[] image;

    public long getVaccin_id() {
        return vaccin_id;
    }

    public void setVaccin_id(long vaccin_id) {
        this.vaccin_id = vaccin_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getVaccin_date() {
        return vaccin_date;
    }

    public void setVaccin_date(Date vaccin_date) {
        this.vaccin_date = vaccin_date;
    }

    public boolean isComplet() {
        return complet;
    }

    public void setComplet(boolean complet) {
        this.complet = complet;
    }

    public String getVaccin_type() {
        return vaccin_type;
    }

    public void setVaccin_type(String vaccin_type) {
        this.vaccin_type = vaccin_type;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Vaccin{" +
                "vaccin_id=" + vaccin_id +
                ", username='" + username + '\'' +
                ", vaccin_date=" + vaccin_date +
                ", complet=" + complet +
                ", vaccin_type='" + vaccin_type + '\'' +
                '}';
    }
}
