package com.example.vaccinservice.repositories;

import com.example.vaccinservice.models.Vaccin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VaccinRepository extends JpaRepository<Vaccin,Long> {

    public Vaccin findByUsername(String username);

    public List<Vaccin> findAllByUsername(String username);

}
