package util;

import com.example.vaccinservice.models.Vaccin;
import org.springframework.context.ApplicationEvent;

public class OnCreateVaccinEvent extends ApplicationEvent {
    private String appUrl;
    private Vaccin vaccin;
    public OnCreateVaccinEvent(String appUrl, Vaccin vaccin) {
        super(vaccin);
        this.appUrl = appUrl;
        this.vaccin = vaccin;
    }
    public String getAppUrl() {
        return appUrl;
    }
    public Vaccin getVaccin() {
        return vaccin;
    }
}
